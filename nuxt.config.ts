// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  devtools: { enabled: true },
  css: ['/assets/css/bulma.css'],
  modules: [
    '@nuxt/content'
  ],
  content: {
    documentDriven: true,
    experimental: {
      clientDB: true
    },
  },
});
