import os
import requests
from gpt4free import you


def get_title(message):
    # get an answer
    prompt_msg="Napiši kratek naslov za spodnje besedilo s čim manj besedami. Ne uporabljaj nobenih dodatnih znakov (narekovajev, ipd.), samo zapiši naslov.```\n\n" + message + "```\n\nNaslov:"
    
    response = you.Completion.create(
    prompt=prompt_msg,
    detailed=False,
    include_links=False)

    response = response.model_dump()["text"]

    response = response.replace("\\u010d", "č").replace("\\u0161", "š").replace("\\u017e", "ž").replace("\\u017d", "Ž").replace("\\u0160", "Š").replace("\\u010c", "Č")

    if "Please try again" in response:
        return None

    return response


ACC_TOKEN = os.getenv('FB_TOKEN')
if ACC_TOKEN is None:
    # Read the token from file
    with open('../.priv', 'r') as f:
        ACC_TOKEN = f.read()

POST_URL = "https://graph.facebook.com/v15.0/me/posts?access_token=" + ACC_TOKEN

POSTS_DISK_PATH = "../content/posts/"

TAGS_WORDS = {
    "pohod": {
        "pohod",
        "hodil",
        "tura",
        "turo",
    },
    "izlet": {
        "izlet",
    },
    "plezanje": {
        "plezanje",
        "plezal",
    },
    "obvestila": {
        "obveščam",
        "obvestilo",
    },
    "tekmovanja": {
        "tekmovanj",
        "tekma",
        "nastop",
    },
}


def download_image(url, title):
    """
    Downloads image from url and returns local path to it
    """

    # Get the image from url
    image = requests.get(url)

    # Get the image name from url
    # Hash the image name
    title = title.replace(" ", "").replace(".", "")[:10]
    image_name = title + str(hash(url))[:6] + '.jpg'

    # Save the image in ../static/images/posts/
    with open(f'../public/img/uploads/{image_name}', 'wb') as f:
        f.write(image.content)

    # Return the local path to the image
    return f'/img/uploads/{image_name}'



def sanitaze_title(title, max_len=6):
    title = title.strip()


    # Remove emojis from title
    title = ''.join(c for c in title if c <= '\uFFFF')
    title = title.replace(":", "").replace(";", "").replace("\"", "").replace("'", "")

    # Remove trailing spaces
    title = title.strip()
    while "  " in title:
        title = title.replace("  ", " ")
    
    title = title.replace(".", "").replace("!", "").replace("?", "")

    # If longer than x words, end it with " ..."
    if len(title.split(' ')) > max_len:
        title = ' '.join(title.split(' ')[:6]) + ' ...'

    return title


def fix_content_typos(content):
    i = 0
    while i < len(content) - 2:
        # Try to find ",x" where x can be any character and replace it with ", x"
        if content[i] in [',', '.', '!', '?'] and content[i+1] != ' ':
            content = content[:i+1] + ' ' + content[i+1:]
        
        # Check if character is not capital letter after . or ! or ?
        if content[i] in ['.', '!', '?'] and content[i+1].islower():
            content = content[:i+2] + content[i+2].upper() + content[i+3:]

        # Remove double spaces
        if content[i] == ' ' and content[i+1] == ' ':
            content = content[:i+1] + content[i+2:]
        

        i += 1

    # Add "." at the end of the content if needed
    if content[-1] not in ['.', '!', '?']:
        content += '.'
    return content    



# Fetch the posts json from url
posts = requests.get(POST_URL).json()

# Load the template post in templates/post.md
with open('templates/post.md', 'r') as f:
    template = f.read()

    # Loop through the posts
    for post in posts['data']:
        if 'message' in post:
            message = fix_content_typos(post['message'])

            #card Set title to first sentence (can end with . or ! or ?)
            title = message.split('.')[0].split('!')[0].split('?')[0]
            title = sanitaze_title(title)

            # Find " " each 110 characters and replace it with "\n"
            # Loop through the message by 110 characters
            inc = 110
            i = 110
            while i < len(message):
                # If the character is space
                if message[i] == ' ':
                    # Replace it with new line
                    message = message[:i] + '\n' + message[i+1:]
                    inc = 110
                else:
                    # Increase i only for 1
                    inc = 1
                i += inc
        

            # Parse date from post date
            date = post['created_time']

            # Get tags from message
            tags = {}
            content_lower = message.lower()
            for word in TAGS_WORDS:
                for tag_word in TAGS_WORDS[word]:
                    if tag_word in content_lower:
                        tags[word] = word
            tags = list(tags)



            filename = title.replace(" ", "_").replace(".", "").replace("!", "").replace(",", "").replace("?", "").lower().replace("č", "c").replace("š", "s").replace("ž", "z")

            # Replace all non ascii characters
            filename = ''.join(c for c in filename if c <= '\u00FF')


            # Remove last char if "_"
            if filename[-1] == '_':
                filename = filename[:-1]

            # If file with the same name exists, skip it
            if os.path.isfile(f'{POSTS_DISK_PATH}{filename}.md'):
                continue

            # Get title from message
            post_title = get_title(message)
            if post_title is None:
                post_title = title
            else:
                post_title = sanitaze_title(post_title, max_len=15)
            print(f"Processing post: {post_title}")

            # Fill {title}, {date}, {body} in the template from post
            content = template.format(
                title=post_title,
                date=date,
                fb_id=post['id'],
                body=message,
                tags=tags
            )

            # Insert images from post
            attachments_url = "https://graph.facebook.com/v17.0/" + post['id'] + "/attachments?access_token=" + ACC_TOKEN
            attachments = requests.get(attachments_url).json()

            for attachment in attachments['data']:
                if 'media' in attachment:
                    content += '\n\n## Slike'
                    content += "\n\n![](" + download_image(attachment['media']['image']['src'], filename) + ")"

                    # Also add header_img: "" after fb_id line
                    content = content.replace("fb_id: " + post['id'], "fb_id: " + post['id'] + "\nheader_img: " + download_image(attachment['media']['image']['src'], filename))

                # Add subattachments
                if 'subattachments' in attachment:
                    content += '\n'

                    # Skip 1. subattachment, as it's the same as the main one
                    for subattachment in attachment['subattachments']['data'][1:]:
                        if 'media' in subattachment:
                            # Create table of images
                            image_src = subattachment['media']['image']['src']
                            content += " ![](" + download_image(image_src, filename) + ")\n"



            with open(f'{POSTS_DISK_PATH}{filename}.md', 'w') as f:
                f.write(content)
        
