---
title: PD Grosuplje
link_title: O društvu
---

| PLANINSKO DRUŠTVO GROSUPLJE    |
|--------------------------------|
| Adamičeva 29 , 1290 Grosuplje  |
| Matična št: 5262984            |
| Davčna št. 21671559            |



## Splošne informacije

* Uradne ure: PONEDELJEK ob 20:00 uri
* Predsednik društva: Franc Štibernik tel. št 041 696 940
* Namestnik, Načelnik Plezalne sekcije Ascendo: Sandi Pelko
* Predsednik nadzornega odbora: Vanja Špelko
* <TrrBank></TrrBank>

Ustanovljeno 12. oktobra 1984 leta.

email: info@pdgrosuplje.si


V društvo je včlanjenih cca 400 članov.
Društvo je takoj po ustanovitvi odprlo svojo planinsko pot in jo poimenovalo *Grosupeljska planinska pot*.
Dnevnik približno 80 km dolge poti ima 14 kontrolnih točk.
Pot vodi po najzanimivejših predelih hribov okrog Grosupeljske kotline in pohodnike seznanja s pomembnimi
zgodovinskimi, kulturnimi in turističnimi točkami občine Grosuplje in sosednjih občin.

## Aktivnosti društva

Društvo je aktivno na naslednjih področjih:

1. Vzgoja vodnikov za varno vodenje v gore in nabava opreme za varovanje.
2. Delo z mladimi: -pomoč vrtcem in šolam pri delu planinskih krožkov. Starši, ki v šolah niso bili seznanjeni, da se otroci lahko vključijo v planinski krožek, se lahko javijo predsedniku društva, ki bo v primeru večjega zanimanja organiziral krožek v okviru društva.
3. Izleti in pohodi po sredogorju po letnem programu izletov.
4. Ture v visokogorje po najzahtevnejših poteh in brezpotju po dogovoru na društvenih sestankih, ki so vsak ponedeljek ob 20 uri v prostorih društva na Adamičevi 51.
5. Plezanje do IV težavnostne stopnje po dogovoru na sestankih.
6. Zimske ture v visokogorje po programu in dogovorih zainteresiranih.
7. Turno smučanje po dogovoru.


Leta 2000 je bila na pobudo društva zgrajena umetna plezalna stena v novi telovadnici Brinje.
Zanimanje za plezanje je bilo veliko zato so člani, 13.5. 2002 ustanovili plezalno sekcijo z imenom ASCENDO.
Leta 2004 pa smo na podstrešju telovadnice na Adamičevi 29 v Grosupljem,
s prostovoljnim delom in pomočjo sponzorjev, zgradili društvene prostore z veliko plezalno steno


Vse ljubitelje narave, pohodništva, alpinizma, turnega smučanja in športnega plezanja, vabimo, da se nam pridružite.
Pridite na društveni sestanek ali pokličite po telefonu (041 696 940 – Franc Štibernik).
