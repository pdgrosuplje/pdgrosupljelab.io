---
title: Grosupeljska pot
---

**VABIMO VAS DA PREHODITE GROSUPELJSKO PLANINSKO POT**

![](/img/uploads/IMG_6635-300x169.jpg)

**Nove markacije za dvonamensko pot.**

 

Vedno in vsem ni mogoče zahajati v visoke gore, zato smo se člani Planinskega društva Grosuplje kmalu po ustanovitvi odločili , da po gričih in dolinah  okrog Grosupljega označimo pot, ki bo planincem različnih starosti in sposobnosti v vseh letnih časih omogočala sproščeno gibanje v  naravi. Našo zamisel smo s prostovoljnim delom uresničili leta 1986, ko smo pot dokončno uredili. Pri urejanju poti nas je vodila tudi želja, da spoznate naše kraje, našo mirno dolenjsko pokrajino.

Skupna dolžina poti je cca 80 km in se jo zlagamo lahko prehodi v štirih delih. Danes pot ne vodi samo po občini Grosuplje ampak posega tudi na področje občin Ivančna gorica, Ljubljane, Škofljice in Velikih Lašč.
Spoznali boste najpomembnejše znamenitosti Grosupeljske kotline in se povzpeli na najvišje vrhove , ki obkrožajo to kotlino. Pogledi pa vam bodo segli tudi daleč naokoli od Snežnika preko Gorjancev, Zasavskih gora, Kamniških planin, Karavank do Julijcev.

Pot je od leta 2015  markirana kot dvonamenska in tako odprta tudi za gorske kolesarje 

Pot je podrobno opisana v vodniku Grosupeljske poti. Za tiste ki vodite evidenco o prehojeni poti je na voljo tudi Dnevnik poti. Za celotno prehojeno pot dobite na osnovi potrjenega dnevnika tudi značko poti.

V društvu smo si zastavili  nalogo, da čim več  pohodnikov organizirano  popeljemo po tej poti.
V štirih sobotah bomo prehodili celotno pot in si v dnevnik Grosupeljske poti nabirali žige.

Kdaj so pohodi si poglejte v Programu pohodov društva.

 

<a href="/files/grosupeljska_pot.gpx" target="_blank">Prenesite GPX datoteko</a>

Pot je označena na karti občine Grosuplje v merilu 1:25000

![](/img/uploads/grosupeljska-pot_clip_image002.jpg)

Ostale poti in pohode lahko najdete na povezavi https://mapzs.pzs.si/.
