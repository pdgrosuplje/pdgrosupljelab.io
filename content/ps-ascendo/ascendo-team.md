---
title: Ascendo Team
---

## Člani upravnega odbora PS Ascendo

* Sandi Pelko, načelnik, referent za tekmovalni šport
* Teja Kovač, namestnica in tajnica
* David Tomažin, referent za vpise otrok
* Lučka Jere, referent za delo z mladimi
* Gregor Germovšek, referent za rekreacijo

## Zasnova in realizacija spletne strani

* Žiga Janež
* Samo Hribar

