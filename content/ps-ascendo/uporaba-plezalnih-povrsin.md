---
title: Uporaba plezalnih površin
---

**HIŠNI RED ZA PROSTORE NA ADAMIČEVI 29**

1. Prostore lahko uporabljajo člani Planinskega društva oziroma PZS, ob prisotnosti pooblaščenega člana PD Grosuplje.
2. V čevljih je vstop dovoljen samo v predprostor in klubski prostor
3. Kajenje v prostoru ni dovoljeno
4. Dostop do plezalnih površin lahko dovoli samo odgovorni vodja plezanja
5. Vsak pleza na lastno odgovornost, pri tem mora spoštovati pravila plezanja in napotke vodje plezanja
6. Uporabniki morajo vzdrževati red in čistočo in pospraviti za seboj
7. Z glasbenim stolpom lahko upravlja samo odgovorni vodja plezanja
8. Odgovorni vodja je odgovoren za pregled objekta ob prihodu in za evidentiranje morebitnih pomanjkljivosti ter za pravilno uporabo in vzdrževanje prostorov
9. Odgovorni vodja ob odhodu pregleda celoten prostor, zapre vsa okna, ugasne ventilator, zapre vse pipe za vodo, zabeleži morebitne poškodbe nastale v njegovi prisotnosti, pogasi luči in zaklene prostor
10. Ključev prostora ni dovoljeno posojati nepooblaščenim osebam
11. Neizvajanje hišnega reda je hujša kršitev pravil društva

Predsednik PD Grosuplje
