---
title: Rojstni dnevi
---

Na Plezalni sekciji ASCENDO organiziramo plezalno praznovanje rojstnega dne za otroke s strokovno vodeno plezalno animacijo na naši
[*bolderci*](/plezalnice/bolder-adamiceva).
Datumi niso fiksno določeni, pač pa jih v skladu z vašimi željami skupaj določimo (praviloma sobote in nedelje).

Pišite nam na info@ascendo.si in se 
bomo dogovorili za datum in možnost izvedbe.



## Pogoji

Da bo rojstni dan potekal varno in sproščeno, moramo upoštevati sledeča pravila:

1. Rojstni dan na naši plezalnici lahko praznujejo vsi otroci, *ne glede na to ali so člani Planinskega društva Grosuplje ali ne*
2. Otroci člani PD Grosuplje imajo prednost pred nečlani, a le do **3 tednov** pred izvedbo rojstnega dne
3. Vsi otroci, praznujoči in povabljeni na rojstni dan morajo biti nujno stari več kot **6 let**
4. V kolikor ima povabljeni bratca ali sestrico mlajšo od **6 let**, morajo biti starši zraven
5. Na zabavi mora vedno biti prisoten **vsaj 1 starš** in **maksimalno 2 starša**
6. Na rojstnem dnevu je zaradi logistike, velikosti prostora in splošne varnosti, lahko **največ 15 otrok**


## Čas izvedbe

Moramo vas opozoriti, da rojstnega dne ni možno rezervirati za manj kot 3 ure,
saj smo iz preteklih praks videli, da sta 2 uri premalo za kvalitetno zabavo.


## Plačilo

Rezervacijska ura znaša **50€** (to vključuje najem prostora in vaditeljevo animacijo).
Plača se vnaprej na TRR računa Planinskega društva Grosuplje (podatki so izpisani spodaj).
Plačati je treba do **2 dni pred** izvedbo rojstnega dneva,
sicer ga *ne bomo izvedli!*

<hr>

<TrrBank>
</TrrBank>

Namen nakazila: ime in priimek otroka/ROJSTNI DAN

<hr>


Upam, da nismo izpadli pregrobi z vsemi pogoji, ampak skozi leta in leta izkušenj in izvedb različnih dejavnosti,
so se nam postavljeni zgornji pogoji izkazali za ključne elemente dejansko kvalitetnih dogodkov.
Počutite se svobodne rezervirati termin, ko bo za to prišel čas oz.
ko boste vi in vaš otrok želeli svoj najljubši praznik praznovati na plezalni steni pri nas. 🙂


Ekipa PS Ascendo
