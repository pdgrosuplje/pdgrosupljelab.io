---
title: PS Ascendo
link_title: Osnovni podatki
---

| PLANINSKO DRUŠTVO GROSUPLJE, PLEZALNA SEKCIJA ASCENDO                          |
---------------------------------------------------------------------------------|
| Uradne ure: PONEDELJEK 20:00 do 21:00                                          |
| Adamičeva 29, 1290 Grosuplje                                                   |
| Predsednik PD Grosuplje: Franc Štibernik tel.: 041 696 940                     |
| Načelnik PS Ascendo: Sandi Pelko tel.: 040 609 509                             |
| <TrrBank></TrrBank>                                                            |
| email: info@ascendo.si                                                         |



V Planinskem društvu Grosuplje (PD) smo se zbrali vsi, ki se že vrsto let ukvarjamo s športnim plezanjem,
in se združili v plezalno sekcijo Ascendo (kar pomeni *plezati* po latinsko).
Za pridobitev, kot je velika plezalna stena v Športni dvorani Brinje in za zaupanje le-te v oskrbo, smo občini izredno
hvaležni.

Plezanje poteka na urejenih plezalnicah:
<Plezalnice></Plezalnice>


V popoldanskem času poteka na obeh stenah večkrat tedensko plezanje za otroke na terminih, ki so prilagojeni njihovi
starosti:
* plezalni vrtec
* začetni trening
* mladinska rekreacija
* večerna mladinska rekreacija

ter bolj zahtevni treningi v skupinah
* napredni trening
* nadaljevalni trening
* tekmovalni trening

Posebnost je družinski termin, kjer lahko plezajo otroci skupaj s starši.

V večernih urah potekajo termini za rekreacijo odraslih.
Vsako leto organiziramo tudi tečaje za pripravnika športnega plezanja.
Do sedaj je že več kot 70 tečajnikov uspešno zaključilo ta program in si pridobilo naziv *pripravnik športnega
plezanja*.
Ta naziv je osnova za nadaljnje izobraževanje do nazivov športnega plezalca in inštruktorja športnega plezanja;
zadnji je nujno potreben za delo z otroci ter njihovo izobraževanje v športu.