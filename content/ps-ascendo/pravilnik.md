---
title: Pravilnik
---

PLANINSKO DRUŠTVO GROSUPLJE
ADAMIČEVA 29
1290 Grosuplje

V skladu z 34. Členom Pravil planinskega društva Grosuplje,
je UO PD GROSUPLJE na svoji seji dne 13.05.2002 sprejel sledeči pravilnik.

## Navodila za delo Plezalne sekcije Ascendo

 

### 1. člen

Plezalna sekcija ASCENDO je ustanovljena z namenom organiziranja naslednjih dejavnosti:


* Izobraževanje in usposabljanje športnih plezalcev
* Treniranje mladih športnih plezalcev
* Organizacija tekmovanj v športnem plezanju
* Organiziranje drugih oblik telesne aktivnosti za vzdrževanje fizične kondicije
* Organiziranje izletov in družabnih srečanj
* Gradnja in vzdrževanje plezališč
* Izdaja strokovne literature
* Opravljanje drugih dejavnosti, ki so v interesu planinskega društva

### 2. člen

Plezalna sekcija ima svoj znak v obliki plezalca in štampiljko z istim znakom.
 

### 3. člen

Plezalna sekcija nastopa v pravnem prometu z drugimi v imenu PD Grosuplje. Sekcija lahko sklepa pravne posle s tretjimi osebami le na podlagi pisnega pooblastila predsednika PD.
Pravila planinskega društva Grosuplje se smiselno uporabljajo tudi pri delovanju sekcije.


### 4. člen

Član sekcije lahko postane vsakdo, ki sprejema pravila in program planinskega društva Grosuplje, plača članarino in članarino sekcije.
Člane sekcije mlajše od 15let, v organih sekcije zastopajo starši oz.zakoniti zastopniki.

### 5. člen

Člani sekcije so lahko tudi podporni člani – donatorji. Upravni odbor sekcije sprejme poseben sklep, v katerem določi pravice in obveznosti podpornih članov.


### 6. člen

Pravice članov sekcije so da:

* Delujejo in odločajo o delu sekcije
* Vodijo organe sekcije
* Dajejo predloge in pobude za delovanje sekcije
* Koristijo ugodnosti, ki so jim zagotovljene z plačilom članarine


### 7. člen

Organi sekcije so:
* Občni zbor
* Upravni odbor


### 8. člen

Občni zbor je lahko redni ali izredni. Redni občni zbor mora biti enkrat letno, praviloma skupaj z občnim zborom PD.
Izredni občni zbor mora sklicati predsednik sekcije, kadar to zahteva upravni odbor PD ali nadzorni odbor ali tretjina članov sekcije. Če predsednik sekcije ne skliče izrednega občnega zbora v 30 dneh, ga skliče predlagatelj, ki poskrbi tudi za določitev dnevnega reda in gradiva.


### 9. člen

Občni zbor je pristojen, da:

* Sprejema navodila za delo sekcije
* Imenuje upravni odbor sekcije
* Sprejema program dela
* Sprejema finančni in zaključni načrt
* Razpravlja in odloča o poročilih in predlogih upravnem odboru PD
* Voli in razrešuje upravni odbor sekcije


### 10. člen

Delo plezalne sekcije vodi upravni odbor sekcije, ki ga imenuje občni zbor sekcije.
Upravni odbor sestavljajo predsednik, blagajnik, tajnik, referent za rekreacijo, referent za organizacije tekem, referent za treninge.
Mandatna doba članov odbora je štiri leta. Člani so v upravni odbor lahko imenovani večkrat.


### 11. člen

Upravni odbor je pristojen za:

* Izvrševanje sklepov občnega zbora sekcije
* Izvajanje letnega programa dela
* Določanje višine članarine za člane sekcije
* Zagotavljanje materialnih in drugih finančnih sredstev za izvajanje programa sekcije
* Finančno poslovanje sekcije, skladno s pravili o finančnem poslovanju PD
* Vodenje evidence članov in pridobivanje novih članov
* Sprejemanje splošnih aktov, ki niso v pristojnosti občnega odbora


### 12. člen

Finančno poslovanje sekcije poteka preko ŽR planinskega društva. Blagajnik sekcije je dolžan finančno poslovanje voditi v skladu s pravilnikom o finančnem poslovanju PZS in podatke posredovati blagajniku PD.


### 13. člen

Član sekcije je lahko samo član PD Grosuplje s plačano članarino PZS za tekoče leto in plačano dodatno članarino za članstvo v sekciji.
Višino in obliko članarine za sekcijo določi Upravni odbor sekcije in je namenjena izključno za pokrivanje stroškov dejavnosti sekcije.
Upravni odbor je dolžan sprejeti finančni načrt za izvajanje posameznih programov sekcije in zagotavljati pozitivno poslovanje za vsak program.
V primeru negativnega poslovanja je upravni odbor sekcije odgovoren upravnemu odboru PD, ki lahko v skrajnem primeru zaradi neracionalne porabe sredstev prevzame vodenje dela sekcije do sklica izrednega občnega zbora sekcije.


### 14. člen

Javnost finančnega poslovanja je zagotovljena s pravico članov sekcije do vpogleda v finančno materialno dokumentacijo.


### 15. člen

Odredbo dajalec za izvajanje finančnega načrta je predsednik sekcije, v njegovi odsotnosti pa tajnik.


### 16. člen

Nadzor nad finančno materialnim poslovanje sekcije opravlja nadzorni odbor PD Grosuplje v skladu s pravili PD.


### 17. člen

Upravni odbor sekcije je dolžan voditi evidenco o premoženju sekcije v skladu s pravili PD.


### 18. člen

Plezalna sekcija lahko preneha z delovanjem:

*   Po sklepu občnega zbora sekcije
*   Po sklepu upravnega odbora PD Grosuplje

<br>

V primeru prenehanja delovanja sekcije se vse premoženje in finančna sredstva prenesejo na Planinsko društvo Grosuplje.


Predsednik PD Grosuplje:
Franc Štibernik
