---
title: Mladi plezalci PS Ascendo dobro napredujejo v Vzhodni ligi
date: 2023-04-05T17:20:05+0000
fb_id: 501557829985231_527815746181809
header_img: /img/uploads/mladi_plez-61364.jpg
draft: false
tags: ['plezanje', 'tekmovanja']
---

Mladi plezalci PS Ascendo že vrsto let uspešno nastopajo na tekmah vzhodne lige, kjer tekmuje 29 klubov s področja
vzhodne Slovenije. Letos je v tekmovanje vključenih 35 naših tekmovalcev. Po polovici izvedenih tekem je naš klub
na drugem mestu. 
Tekmovalci imajo tako lepo priložnost, da z dobrimi nastopi tudi v nadaljevanju ostanemo vodilni
v ligi. 
Hvala trenerjem za uspešno delo, staršem tekmovalcev pa za pomoč in spodbujanje otrok za udeležbo na
tekmah. 
Predsednik PD Grosuplje 
Franc Štibernik.

## Slike

![](/img/uploads/mladi_plez-61364.jpg)
 ![](/img/uploads/mladi_plez-49444.jpg)
 ![](/img/uploads/mladi_plez-94312.jpg)
 ![](/img/uploads/mladi_plez743015.jpg)
 ![](/img/uploads/mladi_plez449306.jpg)
