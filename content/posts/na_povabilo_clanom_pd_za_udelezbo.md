---
title: Na povabilo članom PD za udeležbo ...
date: 2022-10-16T07:27:40+0000
fb_id: 501557829985231_418166463813405
header_img: /img/uploads/na_povabil-70002.jpg
draft: false
tags: ['pohod', 'izlet']
---

Na povabilo članom PD za udeležbo na izlet v neznano se je odzvalo malo članov. Očitno omejitve zaradi korona vplivajo
tudi na takšna druženja. Vseeno pa se nas je zbralo 18 , večino pa so predstavljali učenci planinske skupine na
šoli LA. Naš cilj je bila Slivnica nad Cerknico. Pohod smo pričeli v Grahovem, in se do vrha prijetno nahodili,
da smo v Domu na Slivnici. ki ga upravljata naša člana  uživali v dobrotah ki jih nudijo. Vrh nam je ponudil lepe
poglede proti Cerkniškemu jezeru, za poglede na daljno okolico pa bo potrebno vrh obiskati kdaj drugič. 
Upam,
da bo na moje nalednj povabilo več odziva saj imajo skupni izleti svoj poseben čar.

## Slike

![](/img/uploads/na_povabil-70002.jpg)
 ![](/img/uploads/na_povabil702919.jpg)
 ![](/img/uploads/na_povabil899517.jpg)
 ![](/img/uploads/na_povabil-52786.jpg)
 ![](/img/uploads/na_povabil455731.jpg)
 ![](/img/uploads/na_povabil591444.jpg)
 ![](/img/uploads/na_povabil332515.jpg)
 ![](/img/uploads/na_povabil-13398.jpg)
 ![](/img/uploads/na_povabil-33817.jpg)
 ![](/img/uploads/na_povabil-26402.jpg)
 ![](/img/uploads/na_povabil-84042.jpg)
