---
title: Članarina PD Grosuplje za leto 2023
date: 2022-12-09T17:47:44+0000
fb_id: 501557829985231_455921173371267
header_img: /img/uploads/obvescamo_339447.jpg
draft: false
tags: ['plezanje', 'obvestila']
---

Obveščamo vas da je od 12. 12. 2022 dalje, že možno poravnati članarino PD Grosuplje za leto 2023. 
Po priporočilu
PZS je višina članarina naslednja:
                   
A član z večjim obsegom ugodnosti 68, 00 € 
A/z družinskim
popustom         61, 80 € 

B polnoletna oseba,             32, 00 € 
B/z družinskim popustom         25, 80 €

B1 oseba starejša od 65 let,       24, 00 € 
S+Š srednješolec ali študent do 26. let 21, 00 € 
S+Š/z družinskim
popustom         17, 00 € 
P+O predšolski ali osnovnošolski otrok,  9, 00 € 
P+O/z družinskim popustom       
  7, 20 € 

Vabimo vas da članarino poravnate čimpreje in koristite številne ugodnosti, ki jih pridobite s plačilom
članarine. Povabite tudi svoje prijatelje, da se včlanijo v društvo. 
Še posebej vabimo starše otrok, ki so vpisani
v plezalno sekcijo Ascendo, da se tudi sami včlanite v društvo in koristite družinski popust. 
Članarino lahko
poravnate na sedežu društva na Adamičevi 29, Grosuplje ali se dogovorite za plačilo na 041 696 940
Za več informacij
pošljite sporočilo na. info@pdgrosuplje. si 
Spremljajte tudi naše spletne strani.

## Slike

![](/img/uploads/obvescamo_339447.jpg)