---
title: Pohod po poti spominov
date: 2023-03-01T09:51:33+0000
fb_id: 501557829985231_509789037984480
header_img: /img/uploads/v_nedeljo_318807.jpg
draft: false
tags: ['pohod']
---

V nedeljo, 12 marca 2023 vas vabimo na pohod po poti spominov. 
Štart pohoda je ob 8. uri izpred spomenika Dolfetu
Jakhlu na Taborski cesti v Grosupljem(nasproti železniške postaje) 
Pot nas bo vodila mimo spominske plošče na
Cikavi, kjer je padel narodni heroj Jože Kadunc Ibar. Nadaljevali bomo čez vas Sela do Hude Police in po Grosupeljski
planinski poti do spomenika, kjer je padel Stojan Šuligoj.  Od tu se bomo spustili do vasi Bičje in se potem povzpeli
do spomenika na Pecah, ki spominja na osem padlih partizanov, med njimi tudi vaščanov Štibernik Franca in Šerjak
Jožeta iz Pec ter Rebolj Alojzija iz Bičja. 
Ob 11. uri bo pri spomeniku na Pecah krajša spominska prireditev.

Po prireditvi nadaljujemo pohod mimo Ponove vasi do Brezja pri Grosupljem. 
Pohodniki bodo na cca 15 km pohodu
spoznali lepote Šentjurske doline in zgodovino teh krajev. 
Vabljeni vsi, ki želite spoznati zgodovino teh krajev
in spoštujete partizanski boj za svobodo naše domovine. 

Pohod bo vodil Franc Štibernik. Za dodatne informacije
pokličite na 041 696 940.

## Slike

![](/img/uploads/v_nedeljo_318807.jpg)