---
title: Izlet planinske skupine OŠ LA Grosuplje
date: 2023-06-26T09:29:24+0000
fb_id: 501557829985231_571844398445610
header_img: /img/uploads/izlet_plan-91991.jpg
draft: false
tags: ['pohod', 'izlet']
---

Izlet planinske skupine OŠ LA GROSUPLJE: Bohinj , Krnska jezera, Krn, Planina Kuhinja – 24. do 25. junija 2023.

Na OŠ LA Grosuplje je že nekaj let aktivna planinska skupina pod vodstvom neumorne specialne pedagoginje Petre
Skočir, ki ji pri vodenju pomaga predsednik PD Grosuplje, Franc Štibernik. Vsi člani skupine so tudi člani Planinskega
društva Grosuplje. Imeli smo kar nekaj lepih pohodov in se utrjevali za zaključni dvodnevni pohod, ki smo ga izvedli
za zaključek šolskega leta. 
Letos se je skupina pod vodstvom Barbare Štibernik in Petre Skočir odpravila na dvodnevno
turo iz Bohinja. Prvi dan je bil na vrsti vzpon na Komno, z nadaljevanjem do Bogatinskega sedla in spustom do
Krnskih jezer. Na 7 urni poti do planinskega doma pri Krnskih jezerih je skupina spoznavala lepote Komne in se
seznanila z zgodovinskimi ostanki iz 1. svetovne vojne. Prihod v kočo je pokazal, da se je pohodnikov dotaknila
utrujenost. A namestitev na skupnih ležiščih in topla večerja so hitro povrnila veselje še za večerni sprehod
do Krnskega jezera. 
Drugi dan je sončno jutro hitro prebudilo pohodnike in po zajtrku so zagrizli v strmine proti
2244 m visokemu Krnu. Razgledi so postajali vse lepši, vrh Krna pa je razkril veličasten razgled na velik del
naše domovine. Po malici pri Gomiščkovem zavetišču je sledil dolg spust do Koče na planini Kuhinja. Po dobrih
7 urah hoje so se pohodniki v koči naužili dobrot prijaznih oskrbnikov in se kar težko odpravili do avtobusa,
ki jih je zapeljal v objem svojih staršev v Grosupljem. 
Vsi so se strinjali, da je bil vikend prekrasen. 
Po
pripovedi žene prispevek zapisal Franc Štibernik.

## Slike

![](/img/uploads/izlet_plan-91991.jpg)
 ![](/img/uploads/izlet_plan525932.jpg)
 ![](/img/uploads/izlet_plan162160.jpg)
 ![](/img/uploads/izlet_plan657713.jpg)
 ![](/img/uploads/izlet_plan-48158.jpg)
 ![](/img/uploads/izlet_plan755331.jpg)
 ![](/img/uploads/izlet_plan-75297.jpg)
 ![](/img/uploads/izlet_plan384105.jpg)
 ![](/img/uploads/izlet_plan-27643.jpg)
 ![](/img/uploads/izlet_plan808210.jpg)
 ![](/img/uploads/izlet_plan-43569.jpg)
 ![](/img/uploads/izlet_plan433736.jpg)
