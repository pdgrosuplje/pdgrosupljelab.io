---
title: Mladi plezalci uživajo na tedenskem taboru s Sandijem Pelkom
date: 2023-06-26T16:11:57+0000
fb_id: 501557829985231_572012798428770
header_img: /img/uploads/ne_samo_ml403750.jpg
draft: false
tags: ['pohod', 'plezanje']
---

Ne samo mladi planinci, ki so opravili zahteven pohod, tudi mladi plezalci trenutno uživajo na tedenskem Plezalnem
taboru pod vodstvom načelnika plezalne sekcije Sandija Pelka . Več po zaključku tabora.

## Slike

![](/img/uploads/ne_samo_ml403750.jpg)
 ![](/img/uploads/ne_samo_ml-89336.jpg)
 ![](/img/uploads/ne_samo_ml-70047.jpg)
 ![](/img/uploads/ne_samo_ml-84088.jpg)
 ![](/img/uploads/ne_samo_ml432944.jpg)
 ![](/img/uploads/ne_samo_ml-77302.jpg)
 ![](/img/uploads/ne_samo_ml-90533.jpg)
 ![](/img/uploads/ne_samo_ml553645.jpg)
