---
title: Planinsko društvo Grosuplje spodbuja športno plezanje med otroki
date: 2023-01-27T11:26:31+0000
fb_id: 501557829985231_489212606708790
header_img: /img/uploads/v_planinsk-63209.jpg
draft: false
tags: ['plezanje']
---

V planinskem društvu Grosuplje, namenjamo veliko pozornosti tudi sodelovanju z vrtci in šolami . Otroci radi prihajajo
v našo plezalnico na Adamičevi 29 v Grosupljem in tam spoznavajo čare športnega plezanja. Lepo je videti uživanje
otrok na oprimkih. V tem tednu smo gostili tri skupine učencev iz OŠ Šmarje Sap in dve skupine otrok iz vrtca
Kobacaj.  Zanimanje staršev za vpis otrok v plezalne skupine presega naše možnosti. Veliko otrok čaka na možnost
vpisa, zato otrokom šol in vrtcev omogočamo brezplačne obiske plezalnice na Adamičevi. Za prijetno počutje otrok
je zaslužen Franc Štibernik, ki rad ustreže željam učiteljev in vzgojiteljic.

## Slike

![](/img/uploads/v_planinsk-63209.jpg)
 ![](/img/uploads/v_planinsk-57924.jpg)
 ![](/img/uploads/v_planinsk827576.jpg)
 ![](/img/uploads/v_planinsk-84886.jpg)
 ![](/img/uploads/v_planinsk567231.jpg)
 ![](/img/uploads/v_planinsk-73888.jpg)
 ![](/img/uploads/v_planinsk-52379.jpg)
