---
title: Program pohodov PD Grosuplje za leto 2023
date: 2022-11-21T17:41:23+0000
fb_id: 501557829985231_443727134590671
header_img: /img/uploads/spostovani-64946.jpg
draft: false
tags: ['pohod', 'izlet']
---

Spoštovani člani, pripravljam program pohodov društva za leto 2023. Načrtujemo, da bi vsak mesec izvedli eno akcijo-
izlet. Želim si da bi se člani spet pričeli udeleževati skupnjih tur. Prosim za potrditev, da se zanimate za udeležbo
na izletih, sporočite pa tudi vaše želje, kam bi se radi odpravili. Vesel bom vašega odziva in predlogov na: info@pdgrosuplje.
si ali na franc. stibernik@telemach. net.

## Slike

![](/img/uploads/spostovani-64946.jpg)