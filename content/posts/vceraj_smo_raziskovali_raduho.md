---
title: Raziskovanje Raduhe z vzponom na Veliko Raduho in Lanež
date: 2022-09-24T09:41:14+0000
fb_id: 501557829985231_2555266174614376
header_img: /img/uploads/vceraj_smo588790.jpg
draft: false
tags: ['plezanje']
---

Včeraj smo raziskovali Raduho. Z avtom do kmetije Bukovnik in nato po plezalni smeri na Veliko Raduho. Krasni razgledi
z vrha nato spust do Koče na Loki . Po počitku ponovno vzpon po vzhodni poti na Lanež in čez Durce spust na izhodišče..

## Slike

![](/img/uploads/vceraj_smo588790.jpg)
 ![](/img/uploads/vceraj_smo706509.jpg)
 ![](/img/uploads/vceraj_smo813187.jpg)
 ![](/img/uploads/vceraj_smo-28158.jpg)
 ![](/img/uploads/vceraj_smo-66137.jpg)
 ![](/img/uploads/vceraj_smo376276.jpg)
 ![](/img/uploads/vceraj_smo-72428.jpg)
 ![](/img/uploads/vceraj_smo337841.jpg)
