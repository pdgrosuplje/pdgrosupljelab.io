---
title: Plezalni termini pri Ascendu med počitnicami
date: 2023-02-06T19:53:30+0000
fb_id: 501557829985231_495874846042566
draft: false
tags: ['plezanje', 'obvestila']
---

Lepo pozdravljeni! 
Obveščamo vas, da se v prihodnjem tednu (torej med 6. in 12. februarjem, ko imajo otroci šolske
počitnice) plezalni termini pri Ascendu ne bodo izvajali. S termini ponovno pričnemo s 13. februarjem (ponedeljek).


Želimo vam prijetne počitnice, 
ekipa PS Ascendo.