---
title: Otroci v plezalnici PD Grosuplje
date: 2023-04-13T15:45:00+0000
fb_id: 501557829985231_531885859108131
header_img: /img/uploads/po_praznic-24459.jpg
draft: false
tags: ['plezanje']
---

Po prazničnih dnevih smo na plezalni steni PD Grosuplje tri dni gostili otroke iz vrtcev. V torek so na steni uživali
otroci iz Žalne, ki so za prihod v Grosuplje in nazaj uporabili kar vožnjo z vlakom. V sredo so nas obiskali otroci
iz vrtca na Malem Lipoglavu. Danes pa so kljub dežju na steno prikorakali še otroci iz vrtca Sonček. 
Lepo je gledati zadovoljne otroke kako uživajo, ko spoznavajo čare plezanja.
Svoj prosti čas zato z veseljem namenim otrokom, ki jim vzgojiteljice želijo polepšati dan s plezanjem in igranjem v naši plezalnici.

## Slike

![](/img/uploads/po_praznic-24459.jpg)
 ![](/img/uploads/po_praznic491545.jpg)
 ![](/img/uploads/po_praznic-45917.jpg)
 ![](/img/uploads/po_praznic-68455.jpg)
 ![](/img/uploads/po_praznic298234.jpg)
 ![](/img/uploads/po_praznic-81090.jpg)
