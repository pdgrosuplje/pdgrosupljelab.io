---
title: Planinska tura nad Kočevjem v zimskih razmerah
date: 2023-01-22T18:43:39+0000
fb_id: 501557829985231_486304396999611
header_img: /img/uploads/v_soboto_s-47410.jpg
draft: false
tags: ['pohod']
---

V soboto smo šli z mladimi planinci na prvo zimsko turo. Z vlakom do Kočevja nato pa v hribe nad Kočevjem. Prvi
cilj je bil Fridrihstein, zimsko zasnežen in za mladino uživaško kolovratenje po strmih pobočjih. Drugi cilj je
bi razgledni Mestni vrh in nato uživanje v Koči Pri Jelenovem studencu. Zunaj je med tem pričelo snežiti, tako
da je bil povratek v Kočevje novo doživetje.

## Slike

![](/img/uploads/v_soboto_s-47410.jpg)
 ![](/img/uploads/v_soboto_s-21380.jpg)
 ![](/img/uploads/v_soboto_s620913.jpg)
 ![](/img/uploads/v_soboto_s-19042.jpg)
 ![](/img/uploads/v_soboto_s-57445.jpg)
 ![](/img/uploads/v_soboto_s466128.jpg)
 ![](/img/uploads/v_soboto_s-52961.jpg)
 ![](/img/uploads/v_soboto_s621945.jpg)
 ![](/img/uploads/v_soboto_s386372.jpg)
 ![](/img/uploads/v_soboto_s533863.jpg)
 ![](/img/uploads/v_soboto_s774972.jpg)
 ![](/img/uploads/v_soboto_s-72957.jpg)
