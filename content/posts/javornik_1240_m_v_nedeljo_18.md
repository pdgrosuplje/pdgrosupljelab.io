---
title: Spominski pohod na Javornik
date: 2022-12-10T14:47:42+0000
fb_id: 501557829985231_456574866639231
header_img: /img/uploads/javornik_1-40928.jpg
draft: false
tags: ['pohod']
---

JAVORNIK 1240 m
 
V nedeljo 18. 12. 2022 vabimo na tradicionalni spominski pohod na Javornik. Štart pohoda je s
parkirišča v Črnem vrhu nad Idrijo. Potrebna je zimska oprema glede na razmere na dan pohoda. Hodili bomo skupaj
5 ur. Obvezne so predhodne prijave
Avtobus bo organiziran če bo prijav več kot 15. V primeru, da bo prijav manj
kot pet pohod odpade. 
Prijavite se čim preje. Prijave sprejemamo do torka 13. 12. 2022 na mail: info@pdgrosuplje.
si ali na telefon 041 696 940.

## Slike

![](/img/uploads/javornik_1-40928.jpg)