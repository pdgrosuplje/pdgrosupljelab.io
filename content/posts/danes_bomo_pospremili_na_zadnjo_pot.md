---
title: Slovo Franca Vidmarja
date: 2023-06-30T11:49:39+0000
fb_id: 501557829985231_574064281556955
draft: false
tags:
  - ostalo
---

Danes bomo pospremili na zadnjo pot Franca Vidmarja, ki je bil velik podpornik aktivnosti PD Grosuplje in še posebej
plezalne sekcije Ascendo. 
Vsem njegovim izrekamo globoko sožalje. 
Namesto cvetja smo donirali dejavnostim Gorske
reševalne zveze Slovenije, ki je bila v pomoč tudi pri njegovi nesreči. 
Za vedno boš ostal v naših srcih.