---
title: Izlet na Peco z vzponom na Knipsovo sedlo
date: 2022-10-19T07:54:08+0000
fb_id: 501557829985231_420331996930185
header_img: /img/uploads/letosnji_o615443.jpg
draft: false
tags: ['izlet']
---

Letošnji oktobrski dnevi so nam omogočili kar nekaj lepih planinskih izletov. Na zadnjem izletu smo uživali na
Peci. Z avtom do rudnika Topla nato pa strm vzpon na Knipsovo sedlo in vrh Pece. Spust proti koči po zelo zahtevni
poti južnega ostenja. Pece. Za povratek na izhodišče smo si izbrali sprehod do Matjaževe jame in nato raziskovanje
po gozdnih vlakah po katerih se vozijo tudi gorski kolesarji. Ta pot ni markirana.

## Slike

![](/img/uploads/letosnji_o615443.jpg)
 ![](/img/uploads/letosnji_o-11252.jpg)
 ![](/img/uploads/letosnji_o-30373.jpg)
 ![](/img/uploads/letosnji_o-17828.jpg)
 ![](/img/uploads/letosnji_o-72988.jpg)
 ![](/img/uploads/letosnji_o-80480.jpg)
 ![](/img/uploads/letosnji_o-13938.jpg)
 ![](/img/uploads/letosnji_o860106.jpg)
 ![](/img/uploads/letosnji_o-66440.jpg)
 ![](/img/uploads/letosnji_o-38807.jpg)
 ![](/img/uploads/letosnji_o-57592.jpg)
