---
title: Občni zbor Planinskega društva Grosuplje
date: 2023-03-14T07:24:35+0000
fb_id: 501557829985231_516517707311613
header_img: /img/uploads/clani_plan-21097.jpg
draft: false
tags: ['pohod', 'plezanje']
---

Člani Planinskega društva Grosuplje smo se zbrali na rednem občnem zboru društva. Napolnili smo gostinsko sobo
znane grosupeljske gostilne " Pr Vodičarju". Obravnavali in sprejeli smo poročila o delu v letu 2022 in plane
za leto 2023. 
V letu 2022 smo dosegli rekordno število članstva, saj nas je že 420. Glavni utrip delovanju društva
dajejo mladi plezalci, ki so v večini. Članski zbor je s predstavitvami ostalih dejavnosti, od alpinističnih vzponov,
ledenega plezanja in turnega smučanja, popestril Rok Bernot in vzbudil veliko pozornost tudi med starejšimi člani,
ki včasih neupravičeno menijo, da imamo v društvu premalo aktivnosti za starejše. Predsednik Franc Štibernik je
izrazil ponos nad aktivnosmi mladih in povabil starejše člane, da s prijavami na predvidene pohode  prispevajo,
da bo vsak član lahko našel dejavnost v katero se lahko vključi. 
Zbora se je udeležil tudi predsednik MDO, ki
je bil presenečen nad velikim številom mladih na zboru. Pohvalil je delovanje društva in povabil tudi k udeležbi
pri aktivnostih, ki jih pripravlja meddruštveni odbor. 
Pogrešali smo udeležbo župana Občine Grosuplje, od katerega
člani društva pričakujemo pomoč pri realizaciji projekta izgradnje 17 m visoke plezalne stene ob garažni hiši
v Grosupljem. 
Slike zbora povedo, da je društvo na dobri poti razvoja in da ima veliko kadra, ki zagotavlja,
da bo društvo uspešno delovalo tudi v prihodnosti.

## Slike

![](/img/uploads/clani_plan-21097.jpg)
 ![](/img/uploads/clani_plan622716.jpg)
 ![](/img/uploads/clani_plan-37912.jpg)
