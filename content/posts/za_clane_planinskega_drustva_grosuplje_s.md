---
title: Izlet v neznano za člane PD Grosuplje z kosilom in razgledom
date: 2022-09-22T13:14:22+0000
fb_id: 501557829985231_2553536168120710
header_img: /img/uploads/za_clane_p-54308.jpg
draft: false
tags: ['pohod', 'izlet']
---

Za člane Planinskega društva Grosuplje, s plačano članarino za leto 2022, organiziramo v soboto 15. 10. 2022 društveni
izlet v neznano. . 
Z avtobusom se bomo zapeljali na izhodišče pohoda. Po dveh urah lahke hoje  in premaganih
500 višinskih metrov, bomo imeli kosilo na lepi razgledni točki. 
Po kosilu nas čaka še dobro ura spusta po lepi
razgledni poti do avtobusa. 
Ob prijavi se plača prispevek v višini 15 €
Preživite lep dan v družbi planinskih
prijateljev. 
Prijavite se lahko na: info@odgrosuplje. si ali na 041 696 940. Število mest je omejeno zato pohitite
s prijavami.

## Slike

![](/img/uploads/za_clane_p-54308.jpg)