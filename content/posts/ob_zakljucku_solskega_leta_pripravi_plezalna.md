---
title: Tekmovanje za otroke ob zaključku šolskega leta
date: 2023-06-19T10:53:39+0000
fb_id: 501557829985231_568499128780137
header_img: /img/uploads/ob_zakljuc-12994.jpg
draft: false
tags: ['plezanje', 'tekmovanja']
---

Ob zaključku šolskega leta pripravi plezalna skcija klubsko tekmovanje za vse otroke, ki so včlanjeni v temine
plezanja na plezalnih stenah. Otroci so razdeljeni po starostnih skupinah. V šestih skupinah je od 9 do 17 ure
tekmovalo več kot 100 otrok- Vzdušje v dvorani pri OŠ LA na Tovarniški je bilo ob prisotnosti staršev, ki so glasno
navijali in spodbujali mlade tekmovalce, res enkratno. Vsi pa smo ugotavljali, da Grosuplje nujno potrebuje višjo
plezalno steno( višine 16 m), ki bo omogočala tudi tekmovanja v olimpijskih disciplinah športnega plezanja. Društvo
je že dalo pobudo, da se novo steno umesti na zunanjo stran garažne hiše. Upamo, da bo na občini dovolj razumevanja
in da bomo z gradnjo nove stene lahko pričeli že letos.

## Slike

![](/img/uploads/ob_zakljuc-12994.jpg)
 ![](/img/uploads/ob_zakljuc181597.jpg)
 ![](/img/uploads/ob_zakljuc-86070.jpg)
 ![](/img/uploads/ob_zakljuc-19068.jpg)
 ![](/img/uploads/ob_zakljuc851995.jpg)
 ![](/img/uploads/ob_zakljuc252807.jpg)
 ![](/img/uploads/ob_zakljuc857507.jpg)
 ![](/img/uploads/ob_zakljuc148807.jpg)
 ![](/img/uploads/ob_zakljuc-85997.jpg)
 ![](/img/uploads/ob_zakljuc437421.jpg)
 ![](/img/uploads/ob_zakljuc-67018.jpg)
 ![](/img/uploads/ob_zakljuc-38471.jpg)
