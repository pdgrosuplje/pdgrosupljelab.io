---
title: Pohod na Javornik v spomin na Gradnikovo brigado
date: 2022-12-19T10:01:42+0000
fb_id: 501557829985231_463552575941460
header_img: /img/uploads/na_zadnji_894223.jpg
draft: false
tags: ['pohod']
---

Na zadnji letošnji pohod smo se odpravili na Javornik. Gre za spomninsko pohod v spomin na tragično smrt 47. borcev
Gradnikove brigade. Po dolgem času smo spet imeli pravi zimski pohod. Škoda da se ne odzivate na naša povabila
in zamujate čudovite sprehode v naravo. Imeli smo se lepo.

## Slike

![](/img/uploads/na_zadnji_894223.jpg)
 ![](/img/uploads/na_zadnji_-21107.jpg)
 ![](/img/uploads/na_zadnji_535576.jpg)
 ![](/img/uploads/na_zadnji_893078.jpg)
 ![](/img/uploads/na_zadnji_344303.jpg)
 ![](/img/uploads/na_zadnji_-86705.jpg)
 ![](/img/uploads/na_zadnji_-27897.jpg)
