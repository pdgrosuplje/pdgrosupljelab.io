---
title: Planinski izlet na Mestni vrh nad Kočevjem z OŠ LA
date: 2023-01-11T14:11:03+0000
fb_id: 501557829985231_479112864385431
header_img: /img/uploads/v_soboto_2243212.jpg
draft: false
tags: ['izlet']
---

V soboto 21. 1. 2023 vas vabimo na planinski izlet na Mestni vrh nad Kočevjem. Pridružila se nam bo tudi planinska
skupina OŠ LA 
Z vlakom se bomo iz Grosupljega v Kočevje odpeljali ob 7 43, Po Grajski poti se bomo povzpeli do
razvalin gradu Fridrihštajn in osvojili 1034 visoki Mestni vrh. Skupaj bo cca 4 ure hoje. Za povratek bomo lovili
vlak, ki iz Kočevja odpelje ob 13 uri. Dodatne informacije na 041 696 940.

## Slike

![](/img/uploads/v_soboto_2243212.jpg)