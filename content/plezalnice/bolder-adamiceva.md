---
title: Mala stena Adamičeva
---

V času od nastanka sekcije in do danes je število obiskovalcev plezalne stene ter otrok,
ki redno obiskujejo treninge plezanja trikrat tedensko, tako naraslo, da je bilo nujno potrebno poiskati dodatni prostor za vadbo.
Tako smo po precej dela 18. 5. 2013 končno imeli slavnostno otvoritev prenovljenih društvenih prostorov *Plezalnice* (v slengu se imenuje *boulder* stena).

Nova plezalna stena je sestavljena iz različnih naklonov in strehe.
Meri 380 m^2 s približno 5000 oprimki in stopi in bo razveselila še tako zahtevne plezalce.

Mala plezalna stena je nujno potrebna za resnejše delo z otroci ter velika pridobitev za zahtevnejše rekreacijsko plezanje.
Namenjena je treningom in plezanju zahtevnejših rekreacijski plezalcev,
ki želijo izboljšati svojo telesno pripravljenost predvsem v smislu povečanja maksimalne moči in vzdržljivosti.

## Termini za otroke in mladino

* plezalni vrtec
* začetni trening
* mladinska rekreacija
* mladinska večerna rekreacija
* nadaljevalni trening
* tekmovalni trening

## Termini za odrasle

* rekreacija za odrasle


## Dostop

Plezalnica se nahaja na podstrehi telovadnice OŠ Luisa Adamiča v Grosuplju (Adamičeva 29).
Vhod je na skrajni levi strani objekta, do njega vodijo zunanje stopnice.

Iz Ljubljane se peljemo po avtocesti proti Novemu mestu.
Na drugem izvozu, z napisno tablo: Grosuplje – hotel zapeljemo dol, mimo hotela.
Na velikem križišču zavijemo desno – v Grosuplje.
Do male plezalne stene peljemo mimo dvorane Brinje, nato še cca. 500 metrov, mimo Petrola, do “T” križišča.
Tu zavijemo desno in smo pred plezalnico. Parkiramo na dvorišču šole.

<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=14.655171632766724%2C45.9558713193982%2C14.661367535591125%2C45.958129430431484&amp;layer=mapnik&amp;marker=45.95700038641723%2C14.658269584178925" style="border: 1px solid black"></iframe>

[Prikaži zemljevid](https://maps.google.com/maps/dir//Adami%C4%8Deva+cesta+29+1290+Grosuplje/@45.9570513,14.6587461,14z/data=!4m5!4m4!1m0!1m2!1m1!1s0x476525dfe6b63859:0x135fc9d2961ebc8a)
