---
title: Velika stena Brinje
---

V Športni dvorani Brinje se nahaja velika “reliefna” stena (visoka 9.9m in široka 5m),
ki je sestavljena iz previsnih delov z različnimi nakloni in ravnih stranskih delov in je od Ljubljane oddaljena le 15 km.
Celotna stena znaša okoli 80m² plezalne površine.
Najbolj previsni del stene ima približno 4m naklona glede na maksimalno višino,
kar omogoča postavitev smeri za tekme državnega pokala za kategorije starejši dečki/starejše deklice in mlajši.
Velika stena je poleg tekem namenjena predvsem rekreacijskemu plezanju otrok in odraslih.
Na tej steni se izvajajo tudi redni treningi in tečaji plezanja.

## Tehnični podatki
* Višina: 9,9m
* Širina: 5m
* Plezalna površina: 80m^2
* Maksimalni naklon: 4m



## Termini za otroke in mladino

* začetni trening
* napredni trening
* nadaljevalni trening
* tekmovalni trening
  
**Opozorilo**: v času ligaške tekme termin odpade.
Tisti, ki prihajate prvič, predhodno pokličite vodjo plezanja.
V dogovorjenih terminih lahko plezajo samo člani s poravnano letno članarino PD oziroma PZS
in veljavnim kartončkom o poravnani članarini PS Ascendo.

## Termini za odrasle

* rekreacija za odrasle


## Dostop

Velika dvorana Brinje – Ljubljanska cesta 40a, Grosuplje

Iz Ljubljane se peljemo po avtocesti proti Novemu mestu. Na drugem izvozu, z napisno tablo: Grosuplje – Hotel zapeljemo dol, mimo hotela.
Na velikem krožišču zavijemo desno – v Grosuplje. Po cca. 300 metrih je na levi dvorana Brinje, pod njo pa veliko parkirišče.

<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=14.65617209672928%2C45.96477083882633%2C14.66236799955368%2C45.967028587179676&amp;layer=mapnik&amp;marker=45.9659%2C14.659269999999992" style="border: 1px solid black"></iframe>

[Prikaži zemljevid](https://maps.google.com/maps/dir//Ljubljanska+cesta+40a+1290+Grosuplje/@45.9628206,14.6576441,14z/data=!4m5!4m4!1m0!1m2!1m1!1s0x476525e6e2894a75:0xd62ee95a4070e084)