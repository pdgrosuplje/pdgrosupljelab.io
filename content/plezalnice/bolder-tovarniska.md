---
title: Mala stena Tovarniška
---
V šolskem letu 2020/2021 smo skupaj z novo telovadnico na Tovarniški ulici pridobili tudi novo plezalno steno.
Ta je ravno dovolj velika za eno skupino plezalcev.

## Tehnični podatki

Stena je višja od tiste na [Adamičevi](/plezalnice/bolder-adamiceva), zato je primerna tudi za malo starejše plezalce.

* Višina: 5m
* Širina: 16m
* Plezalna površina: 95m^2

## Termini za otroke in mladino

* mladinska rekreacija
* mladinska večerna rekreacija
* napredni trening
* nadaljevalni trening
* tekmovalni trening

## Dostop

Plezalnica Tovarniška 14, Grosuplje

<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=14.654573500156404%2C45.9520354896741%2C14.660769402980806%2C45.95429375701124&amp;layer=mapnik&amp;marker=45.95316463484511%2C14.657671451568604" style="border: 1px solid black"></iframe>

[Prikaži zemljevid](https://www.openstreetmap.org/directions?from=&to=45.95357%2C14.65726)